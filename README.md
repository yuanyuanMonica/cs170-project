# cs170-project

### How to use the `graph_generator.py`
Run the generate_graph() func in `graph_generator.py` and pass in the following
parameters:  

- students: number of students(# of vertices)  
- buses: number of buses(# of partition)  
- percent_edge_in_cons: percentage of edges in each rowdy group (friendship within each rowdy group)  
- percent_edge_in_cons: percentage of edges between rowdy groups  
- size_group: mean number of people in one rowdy group  
- num_group: number of rowdy group  
- path: path to the output files    
    eg. "./small/" or "./medium" or "./large/"  


**example call**:  
\>>> generate_graph(30, 5, 0.6, 0.2, 5, 6, "./small/")  
\>>> generate_graph(400, 20, 0.5, 0.1, 18, 100, "./medium/")
\>>> generate_graph(800, 40, 0.5, 0.2, 18, 200, "./large/")
