import os
import sys
import networkx as nx
import matplotlib.pyplot as plt
import random
import numpy as np


def generate_graph(students, buses, percent_edge_in_cons, percent_edge_out_cosn, \
					size_group, num_group, path):
	'''
		Inputs:
			students - number of students(# of vertices)
			buses - number of buses(# of partition)

			percent_edge_in_cons: percentage of edges in each rowdy group
				(friendship within each rowdy group)
			percent_edge_in_cons: percentage of edges between rowdy groups
			size_group - mean number of people in one rowdy group
			num_group - number of rowdy group
			path - path to the output files

		Outputs:
			.gml - exported the graph into a .gml file
			.txt - exported the list of rowdy group into a .txt file


		example call:
		=====
		>>> generate_graph(30, 5, 0.5, 0.1, 5, 6, "./inputs/small/59/")
	'''

	#create directory
	if not os.path.exists(path):
		os.makedirs(path)

	#create a new Graph object
	G = nx.Graph()
	#generate corresponding node labels in this graph
	names = generate_node_name(students)
	G.add_nodes_from(names)
	# print(len(names))

	#generate random rowdy groups within all students
	rowdy_groups = generate_rowdy_group(size_group, num_group, students)
	#print(rowdy_groups)

	#write it to the file
	write_parameters_file(path, buses, np.ceil(students / buses), rowdy_groups)


	#add in_group friendship edges in the graph
	in_group_friends = generate_in_group_friendship(percent_edge_in_cons, num_group, rowdy_groups)
	G.add_edges_from(in_group_friends)
	#print(len(in_group_friends))


	#add some random out_group friendship edges in the graph
	out_group_friends = generate_out_group_friends(in_group_friends, percent_edge_out_cosn, students)
	#print(type(out_group_friends))
	#return out_group_friends
	G.add_edges_from(out_group_friends)


	#output graph to .gml file
	output_graph(G, path)

	return G

def generate_out_group_friends(in_group_friends, percent, students):
	"""
	This function generate friends between groups

	input:
		in_group_friends: list of friendship in rowdy groups
		percent: percentage of friendship out of groups
		student: total number of students


	output:
		list of friendship pair to add
	"""
	num_to_add = int(np.round((students * (students - 1) / 2.0) * percent))
	out_group_friends = list(range(num_to_add))
	counter = 0
	while counter != num_to_add:
		a = random.randint(0, students-1)
		b = random.randint(0, students-1)
		while(a == b):
			b = random.randint(0, students-1)

		if ((str(a), str(b)) not in in_group_friends) | \
		((str(b), str(a)) not in in_group_friends):
			out_group_friends[counter] = (str(a), str(b))
			counter += 1

	return out_group_friends



def write_parameters_file(path, num_bus, capa_bus, groups):
	"""
	This function write parameters to a .txt file.
	"""
	file = open(path + 'parameters.txt', 'w')

	str = ""
	str += "{}\n".format(num_bus)
	str += "{}\n".format(int(capa_bus))

	for lst in groups:
		str += "{}\n".format(lst)

	file.write(str)
	file.close()





def generate_node_name(num):
	'''
		Input: num - number of nodes that need a label

		Output: nodes_name - list of strings with all distinct label
	'''
	lst = []
	for i in range(num):
		#print(i)
		lst += [str(i)]

	return lst


def generate_rowdy_group(size_group, num_group, students):
	'''
		Inputs:
			size_group - the number of students in each rowdy group

			num_group - the number of rowdy group in the problem
			students - labels of all the students

		Output:
			rowdy_groups - list of lists that represent each rowdy group with [labels]

		randomly generate the group of students that are in one rowdy group
	'''

	groups = []

	for i in range(num_group):
		r_group = []


		#size of each rowdy group is generated from a normal distribution
		size = np.random.normal(size_group, size_group /10.0)
		size = int(np.round(size))

		while (len(r_group) != size):

			a = random.randint(0, students-1)
			if str(a) not in r_group:
				r_group += [str(a)]

		groups += [r_group]

	return groups



def generate_in_group_friendship(percent, num_group, rowdy_groups):

	'''
		Inputs:
			percent - how many edges out of the total number of edges in each rowdy group (friendship within each rowdy group)
			num_group - number of rowdy groups
			rowdy_groups - list of groups

		Output:
			in_group_friends - list to represent each in_group friendship in each corresponding groups
	'''
	in_group_friends = []

	for i in range(num_group):

		friends_ct = 0
		group = rowdy_groups[i]
		group_size = len(group)
		num_friend_each_group = np.round(((group_size * (group_size-1)) / 2) * percent)

		while (friends_ct != num_friend_each_group):

			a = random.randint(0, group_size-1)
			b = random.randint(0, group_size-1)
			while(a == b):
				b = random.randint(0, group_size-1)
			if ((group[a], group[b]) not in in_group_friends) | \
			((group[b],group[a]) not in in_group_friends):
				in_group_friends += [(group[a], group[b])]
				friends_ct += 1

	return in_group_friends



def output_graph(G, path):
	"""
	This function write the Graph object to a .gml file.
	"""
	file = open(path + 'graph.gml', 'wb')
	nx.write_gml(G, file)
	file.close()


def draw_graph(G):
	'''
	This function will visualize the graph

		param G:
	'''
	nx.draw(G, with_labels=True, font_weight='bold')
	plt.show()
	plt.savefig("tempG.png")