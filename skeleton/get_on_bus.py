import os
import sys
import networkx as nx
import matplotlib.pyplot as plt
import random
import numpy as np


'''
    This file is mainly taking care of bus assignment of each student:


'''


def get_on_bus_func(graph, bus_lst, rowdy_ppl, size_bus):
    '''

    param graph: a graph representing the friendship between students
    param bus_lst: List of bus objects
    param rowdy_ppl: num of ppl that we gave up among in each rowdy group
    param size_bus: capacity of a bus

    return: updated version of bus_lst
    '''

    #calc how many rowdy ppl in the last several buses
    evil_bus_num = int(np.ceil(len(rowdy_ppl) / size_bus))
    evil_bus_left = bus_lst[-evil_bus_num].capacity
    evil_bus_filled = size_bus - evil_bus_left

    evil_bus = bus_lst[-evil_bus_num]
    #TODO how to deal with no most friend bus person
    # #mapping from node to degree of node
    # degree_G = {}
    # for node in list(graph.nodes):
    #     degree_G[node] = graph.degree[node]

    #iter thr all good ppl to put them into the buses
    for node in list(graph.nodes):
        most_friend_bus = find_most_friends_bus(graph, node, bus_lst[0:-evil_bus_num])
        if most_friend_bus == None:
            most_friend_bus = find_not_full_bus(bus_lst)

            if most_friend_bus == None:
                most_friend_bus = evil_bus

        most_friend_bus.add_to_bus(node)




    return None



def find_not_full_bus(bus_lst):
    '''

    :param bus_lst: a full version of bus list
    :return: the one is not full
    '''

    for bus in bus_lst:
        if not bus.is_full():
            return bus






def find_most_friends_bus(graph, node, bus_lst):
    '''

    :param graph: the graph that represents the friendship b/w ppl
    :param node: the node that we want to find most friends bus with
    :param bus_lst: the list of buses that we will look over through
    :return: the bus that has the most friends in
    '''

    max_friends = 0
    max_bus = None
    for bus in bus_lst[:-2]:
        curr_friends = count_friends_on_bus(graph, node, bus)
        if max_friends < curr_friends:
            if bus.is_full():

                continue
            max_friends = curr_friends
            max_bus = bus



    return max_bus





def count_friends_on_bus(graph ,node, bus):
    '''

    :param graph: the graph that represents the friendship b/w ppl
    :param node: the node that we want to know how many friends he/she has on this bus
    :param bus:  the bus that might have his/her friends
    :return: the number of friends that NODE has on BUS
    '''
    count = 0
    for friend in list(graph.adj[node]):
        if friend in bus.passenger_list:
            count += 1
            continue

    return count
