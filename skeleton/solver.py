import networkx as nx
import matplotlib.pyplot as plt
import os
import copy
import get_on_bus as solver2

###########################################
# Change this variable to the path to
# the folder containing all three input
# size category folders
###########################################
path_to_inputs = "./all_inputs"

###########################################
# Change this variable if you want
# your outputs to be put in a
# different folder
###########################################
path_to_outputs = "./outputs"

def parse_input(folder_name):
    '''
        Parses an input and returns the corresponding graph and parameters

        Inputs:
            folder_name - a string representing the path to the input folder

        Outputs:
            (graph, num_buses, size_bus, constraints)
            graph - the graph as a NetworkX object
            num_buses - an integer representing the number of buses you can allocate to
            size_buses - an integer representing the number of students that can fit on a bus
            constraints - a list where each element is a list vertices which represents a single rowdy group
    '''
    graph = nx.read_gml(folder_name + "/graph.gml")
    parameters = open(folder_name + "/parameters.txt")
    #graph = nx.read_gml(folder_name )
    #parameters = open(folder_name)
    num_buses = int(parameters.readline())
    size_bus = int(parameters.readline())
    constraints = []

    for line in parameters:
        line = line[1: -2]
        curr_constraint = [num.replace("'", "") for num in line.split(", ")]
        constraints.append(curr_constraint)

    return graph, num_buses, size_bus, constraints

def solve(graph, num_buses, size_bus, constraints):
    #print(graph.nodes)
    #labels the original constraints
    labeled_constraints = labeled_cons(constraints)
    # generate a list of rowdy_students
    rowdy, graph = relax_constraints(graph, labeled_constraints)
    #generate new constraints and new labeld constraints
    consV1 = generate_new_constraints(rowdy, constraints)
    labeledConsV1 = labeled_cons(consV1)

    # Generate all the buses to store students.
    bus_list = [Bus(size_bus) for i in range(num_buses)]
    # put rowdy students in the end of the bus list
    bus_list = get_rowdy_on_bus(bus_list, rowdy)

    #print(graph.nodes)
    #use solver1 to process the graph
    graph, consV2, bus_list = solver1(graph, labeledConsV1, bus_list, size_bus)
    #use solver2 to process the graph
    solver2.get_on_bus_func(graph, bus_list, rowdy, size_bus)

    #check if any bus is empty
    eliminate_empty(bus_list)

    return bus_list


class Bus:
    def __init__(self, size):
        self.capacity = size
        self.passenger_list = []


    def add_to_bus(self, student):
        if self.capacity >= 0:
            self.passenger_list.append(student)
            self.capacity -= 1
        else:
            raise ValueError("no room for more student.")


    def delete_from_bus(self, student):
        self.passenger_list.remove(student)
        self.capacity += 1
        return student

    def is_full(self):
        #if len(self.passenger_list) >= self.capacity:
        #    return True
        #return False
        return self.capacity <= 0

def eliminate_empty(bus_list):
    index = len(bus_list) - 1
    for bus in bus_list:
        if bus.passenger_list == []:
            while len(bus_list[index].passenger_list) <= 1:
                index -= 1
            stu = bus_list[index].passenger_list.pop(0)
            bus.add_to_bus(stu)




def labeled_cons(constraints):
    """
    This function labels constraints by its number.

    input:
        constraints - a list of list that contains all rowdy groups

    output:
        labeled_cons - a dictionary for labeled constraints
    """
    # Dictionary for labeled constraints
    # Key: 0, 1, 2... Value: [each constraints]...
    labels_of_constraints = list(range(0, len(constraints)))
    labeled_constraints = dict(zip(labels_of_constraints, constraints))
    return labeled_constraints


def get_rowdy_on_bus(bus_list, rowdy):
    """
    This function put students in the rowdy list into the end of the bus list.
    input:
        bus_list - a list of empty buses
        rowdy - a list of rowdy students

    return:
        bus_list - a list of buses with rowdy students on board
    """
    curr = len(bus_list) - 1
    for stu in rowdy:
        if bus_list[curr].capacity <= 0:
            curr -= 1
        bus_list[curr].add_to_bus(stu)

    return bus_list

def relax_constraints(graph, labeled_constraints):
    """
    This function picks students from every rowdy group to relax all constraints.
    The chosen students will be remove from the graph.

    inputs:
        graph - the origin graph
        labeled_constraints - a dictionary of constraints with labels

    outputs:
        rowdy_students - list of rowdy_students that needs to be put into a bus
        updated_graph - the graph after all chosen students removed
    """
    rowdy_students = []
    cons_copy = copy.deepcopy(labeled_constraints)

    #loop until all constraints are relaxed
    while len(cons_copy) != 0:
        #calculate degree / number of apperances for each student
        degree = dict(graph.degree)
        apperances, sorted = generate_Ci(list(graph.nodes), cons_copy)
        deg_over_apper = dict([(key, degree[key] / len(apperances[key])) \
            for key in degree if len(apperances[key]) != 0])

        #find a student with minimum degree / apperances
        mindeg = min(deg_over_apper, key = deg_over_apper.get)

        #add that student to rowdy_students, remove it from the graph and all related
        #constraints
        rowdy_students += [mindeg]
        graph.remove_node(mindeg)
        relaxed = apperances[mindeg]
        for label in relaxed:
            cons_copy.pop(label, None)

    return rowdy_students, graph

def generate_new_constraints(rowdy_students, constraints):
    """
    This function generate a new list of constraints that have one student from each
    rwody group removed.

    input:
        rowdy_students - list of rowdy_students that needs to be put into a bus
        constraints - list of constraints

    output:
        newcons - new list of constraints # NEED TO BE LABELED TO BE USED.
    """
    newcons = []
    for con in constraints:
        newcon = [x for x in con if x not in rowdy_students]
        if newcon != []:
            newcons += [newcon]
    return newcons

def solver1(graph, constraints, bus_list, size_bus):
    """
    This function is the first step of our solver, which uses the relaxed constraints
    as the cluster trying to maximize the score.

    input:
        graph - the updated graph without rowdy_students
        constraints - relaxed constraints without rowdy_students
        bus_list - buslist with rowdy_students in

    output:
        graph - updated graph
        constraints - updated constraints
        bus_list - updated buslist

    """
    # Initialize the ratios for all the constraints
    r_within_init, r_between_init = generate_ratios(graph, constraints)

    labelToRemove = []
    num_buses = len(bus_list)
    # Give the first priority to strongly within-connected constraints
    for label in constraints.keys():
        # High within, not extreamly high between
        if r_within_init[label] > 0.7 and r_between_init[label] < 0.5: #
            # Can fit into a whole bus
            if len(constraints[label]) <= size_bus:
                # Get the first empty bus from current bus_list
                count = 0
                while count < num_buses and bus_list[count].capacity != size_bus:
                    count += 1
                if count < len(bus_list):
                    # Put all part of the constrints into the bus
                    for student in constraints[label]:
                        if student in list(graph.nodes):
                            bus_list[count].add_to_bus(student)
                            graph.remove_node(student) # Remove student from the graph
                    labelToRemove += [label]
            else:
                # If the between is really low
                if r_between_init[label] < 0.25:
                    count = 0
                    while count < num_buses and bus_list[count].capacity != size_bus:
                        count += 1
                    if count < len(bus_list):
                        while bus_list[count].capacity != 0 and constraints[label] != []:
                            student = constraints[label][0] # Select the first student in cons
                            if student in list(graph.nodes):
                                bus_list[count].add_to_bus(student)
                                graph.remove_node(student) # Remove student from the graph
                                constraints[label].pop(0) # Remove student from that constraint
                            else:
                                #if the
                                constraints[label].pop(0) # Remove student from that constraint

    for label in labelToRemove:
        constraints.pop(label)

    # Get a new pair of ratios for the new constrints and graph
    r_within_updated, r_between_updated = generate_ratios(graph, constraints)

    #reinit
    labelToRemove = []
    # For the new graph. Exam the constraints with new paragraph.
    for label in constraints.keys():
        # High within, not extreamly high between, with new parameters
        if r_within_init[label] > 0.5 and r_between_init[label] < 0.3:

            # Check if there is a half-empty bus that can fit this group in
            # But also need to campare the edges between the existing buses

            if len(constraints[label]) <= size_bus:
                currBus = 0
            # Get the first empty bus from current bus_list
                while currBus < len(bus_list) and bus_list[currBus].capacity < len(constraints[label]):
                    currBus += 1
                if currBus < len(bus_list):
                    # Put all part of the constrints into the bus
                    for student in constraints[label]:
                        if student in list(graph.nodes):
                            bus_list[currBus].add_to_bus(student)
                            graph.remove_node(student)
                    labelToRemove += [label]

    for label in labelToRemove:
        constraints.pop(label)

    return graph, constraints, bus_list


# if bus_list[current_Bus].capacity > 0:
#     bus_list[current_Bus].add_to_bus(student) # Add student to the availabel bus
#     student_checklist.remove(student)    # Remove student from the student checklist
#     for c in Constraints_checklist:      # Remove student from the constraints checklist
#         if student in c:
#             c.remove(student)

def generate_ratios(graph, constraints):
    '''
    Inputs:
        graph - the graph as a NetworkX object
        constraints - a dictionary where each element is a list vertices which represents a single rowdy group

    Outputs:
        r_within - Dictionary of within_ratio for each constraint
        r_between - Dictionary of between_ratio for each constraint
    '''
    r_within = {}
    r_between = {}
    e_within = generate_E_within(graph, constraints)
    e_between = generate_E_between(graph, constraints)
    for label in constraints.keys():
        # Number of vertices in each cons
        num_vertices = len(constraints[label])
        # Possible edges within current constraints
        e_within_possible = num_vertices*(num_vertices - 1)/2
        # Possible edges between current constraints with all the others
        e_between_possible = num_vertices*(nx.number_of_nodes(graph))
        # Generate the ratio for current constraints
        if e_within_possible == 0 or e_between_possible == 0:
            r_within[label] = 1
            r_between[label] = 1
        else:
            r_within[label] = e_within[label]/e_within_possible
            r_between[label] = e_between[label]/e_between_possible
    return r_within, r_between




def generate_Di(graph, subgroup, option = "max"):
    '''
		Inputs:
			graph - the graph

		Output:
			sorted_key_list - list of student in the increasing order of its degree
	'''
    Di = graph.degree
    sorted_key_list = sorted(Di, key = lambda k: Di[k])
    #sorted_Di = [{item: Di[item]} for item in sorted_key_list]

    return sorted_key_list

def generate_Ci(students, labeled_constraints):
    """

    """
    ci = dict(zip(students, [[] for i in range(len(students))]))
    for key in labeled_constraints:
        for s in labeled_constraints[key]:
            ci[s].append(key)

    #sorted_Ci_list = sorted(ci, key = lambda k: len(ci[k]), reverse = True)
    sorted_Ci_list = sorted(ci, key = lambda k: len(ci[k]))
    return ci, sorted_Ci_list


def generate_E_within(graph, labeled_constraints):
    '''
        Inputs:
            graph - the graph as a NetworkX object
            labeled_constraints - a dictionary where each element is a list vertices which represents a single rowdy group

        Outputs:
            E_within - a list where each element representing how many edges are within each constraints.
                       the length of the list is the number of constraints
    '''
    within = {}
    for label in labeled_constraints.keys():
        group = labeled_constraints[label]
        count = 0
        for edge in graph.edges(group):
            if edge[1] in group:
                count += 1
        within[label] = count
    return within

def generate_E_between(graph, constraints):
    '''
        Inputs:
            graph - the graph as a NetworkX object
            constraints - a dictionary where each element is a list vertices which represents a single rowdy group

        Outputs:
            E_within - a dictionary where each element representing how many edges are between constraints.
                       the length of dictionary is the number of constraints.
    '''
    between = {}
    for label in constraints.keys():
        group = constraints[label]
        count = 0
        #loop over all edge in this subgroup, count how many are in between constraints
        for edge in graph.edges(group):
            if edge[1] not in group:
                count += 1
        between[label] = count
    return between




# A list where each elements representing how many people are in the bus.
#self.bus_counts = [0 for i in range(num)]
# A list of list to store the student. There are "num" lists, each list with length "size"
#self.bus_assignments = [[None for i in range(size) ] for j in range(num)]

def main():
    '''
        Main method which iterates over all inputs and calls `solve` on each.
        The student should modify `solve` to return their solution and modify
        the portion which writes it to a file to make sure their output is
        formatted correctly.
    '''
    #size_categories = ["small", "medium", "large"]
    size_categories = ["medium"]
    if not os.path.isdir(path_to_outputs):
        os.mkdir(path_to_outputs)

    for size in size_categories:
        category_path = path_to_inputs + "/" + size
        output_category_path = path_to_outputs + "/" + size
        category_dir = os.fsencode(category_path)

        if not os.path.isdir(output_category_path):
            os.mkdir(output_category_path)

        for input_folder in os.listdir(category_dir):
            input_name = os.fsdecode(input_folder)
            graph, num_buses, size_bus, constraints = parse_input(category_path  + "/" + input_name)

            solution = solve(graph, num_buses, size_bus, constraints)
            output_file = open(output_category_path + "/" +  input_name + ".out", "w")

            #TODO: modify this to write your solution to your
            #      file properly as it might not be correct to
            #      just write the variable solution to a file
            str = ""
            for b in solution:
                str += "{}".format(b.passenger_list)
                str += "\n"
            #print(str)
            #return
            output_file.write(str)

            output_file.close()
        #return solution

if __name__ == '__main__':
    main()
